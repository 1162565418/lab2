#include<stdlib.h>
#include<mpi.h>
#include<stdio.h>
 

void printfunc(int x,int y,int *Map) {
	for (int i = 0; i <x; i ++) {
		for (int j = 0; j <y; j ++)
			printf(" %3d ",Map[i*y+j]);
			
		printf("\n");
	}
}
void rule(int rank,int procs,int x,int y,int *Map) {
	
	
	for (int j = 0; j <y; j ++){
		Map[0*y+j]=Map[0*y+j];
		Map[(x-1)*y+j]=Map[(x-1)*y+j];
	}
	for (int ii = 1; ii <x; ii ++) {
		for (int jj = 1; jj <y; jj ++){
			int num=0;
			if(x-rank+ii*procs>jj){
			num  =Map[(ii-1)*y+jj-1]+Map[(ii-1)*y+jj]+Map[(ii-1)*y+jj+1]
			+Map[ii*y+jj-1] +Map[ii*y+jj+1]
			+Map[(ii+1)*y+jj-1]+Map[(ii+1)*y+jj]+Map[(ii+1)*y+jj+1];
			}
			else{
				num  =Map[(ii-1)*y+jj-1]+Map[(ii-1)*y+jj]+Map[(ii-1)*y+jj+1]
			+Map[ii*y+jj-1] +Map[ii*y+jj+1]
			+Map[(ii+1)*y+jj-1]+Map[(ii+1)*y+jj]+Map[(ii+1)*y+jj+1];
			}
			if (Map [ii*y+jj] == 1 && (num == 2 || num == 3) )
				Map [ii*y+jj] = 1;
			else 
				Map [ii*y+jj] = 0;
			if (Map [ii*y+jj] == 0 && num == 3) 
				Map [ii*y+jj] = 1;
		}
	} 
} 

int main (int argc, char** argv) {
	int ty=0,tx=0,x=0,y=0;
    x=atoi(argv[1]);
	y=atoi(argv[2]);
	int rank = 0, procs = 0;
	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
	int *Map=(int*)malloc(sizeof(int)*(x+2)*(y+2));
	if(rank==0){
		for (int i = 0; i <= x; i ++) {
		for (int j = 0; j <= y; j ++){
				Map[i*y+j]=rand()%2;
		}
		}
	}
	tx=x/procs;
	ty=y/procs;
	int *M=(int*)malloc(sizeof(int)*(x+2)*(y+2));
	MPI_Scatter(Map,tx*ty,MPI_INT,M,tx*ty,MPI_INT,0, MPI_COMM_WORLD);
	int iter=1;
	while(1){	
		rule(rank,procs,x,y,M);
		iter=iter+1;
		if(iter==200) break;
	}
	MPI_Gather(M,tx*ty,MPI_INT,Map,tx*ty,MPI_INT,0, MPI_COMM_WORLD);
	if(rank==0){
		printfunc(x,y,Map);
	}
	free(Map);
	free(M);
	MPI_Finalize();
	return 0;
} 
